package pl.marim.openshift.infrastructure.config;

import javax.enterprise.inject.Specializes;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.airhacks.porcupine.configuration.control.ExecutorConfigurator;
import com.airhacks.porcupine.execution.control.ExecutorConfiguration;

@Specializes
public class CustomExecutorConfigurator extends ExecutorConfigurator {

	public static final String PROCESS_CREATE_ORDER_ES = "processCreateOrderES";
	public static final String PROCESS_READ_ORDER_STATUS_ES = "processReadOrderStatusES";

	@Inject
	@ConfigProperty(name="api.request.pool.coreSize", defaultValue="3")
	private Integer apiCorePoolSize;
	
	@Inject
	@ConfigProperty(name="api.request.pool.maxSize", defaultValue="10")
	private Integer apiMaxPoolSize;

	@Inject
	@ConfigProperty(name="api.request.pool.queueCapacity", defaultValue="10")
	private Integer apiQueueCapacity;

	@Inject
	@ConfigProperty(name="api.createOrder.pool.coreSize", defaultValue="10")
	private Integer apiCreateOrderCorePoolSize;
	
	@Inject
	@ConfigProperty(name="api.createOrder.pool.maxSize", defaultValue="15")
	private Integer apiCreateOrderMaxPoolSize;

	@Inject
	@ConfigProperty(name="api.createOrder.pool.queueCapacity", defaultValue="10")
	private Integer apiCreateOrderQueueCapacity;
	
	@Inject
	@ConfigProperty(name="api.readOrderStatus.pool.coreSize", defaultValue="10")
	private Integer apiReadOrderStatusCorePoolSize;
	
	@Inject
	@ConfigProperty(name="api.readOrderStatus.pool.maxSize", defaultValue="15")
	private Integer apiReadOrderStatusMaxPoolSize;

	@Inject
	@ConfigProperty(name="api.readOrderStatus.pool.queueCapacity", defaultValue="10")
	private Integer apiReadOrderStatusQueueCapacity;	
	
	
	@Override
    public ExecutorConfiguration forPipeline(String name) {
    	
    	if (PROCESS_CREATE_ORDER_ES.equals(name)) {
    		return new ExecutorConfiguration.Builder()
    				.abortPolicy()
    				.corePoolSize(apiCreateOrderCorePoolSize)
    				.maxPoolSize(apiCreateOrderMaxPoolSize)
    				.queueCapacity(apiCreateOrderQueueCapacity)
    				.build();
    	} else if (PROCESS_READ_ORDER_STATUS_ES.equals(name)) {
    		return new ExecutorConfiguration.Builder()
    				.abortPolicy()
    				.corePoolSize(apiReadOrderStatusCorePoolSize)
    				.maxPoolSize(apiReadOrderStatusMaxPoolSize)
    				.queueCapacity(apiReadOrderStatusQueueCapacity)
    				.build();
    	}
    	
		return new ExecutorConfiguration.Builder()
				.abortPolicy()
				.corePoolSize(apiCorePoolSize)
				.maxPoolSize(apiMaxPoolSize)
				.queueCapacity(apiQueueCapacity)
				.build();

    }
}
