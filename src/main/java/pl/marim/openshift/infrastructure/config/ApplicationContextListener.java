package pl.marim.openshift.infrastructure.config;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebListener
public class ApplicationContextListener implements ServletContextListener {

	@Inject 
	Banner banner;
	
	@PostConstruct
	private void init() {
	}
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		showContextInfo(sce);
		banner.showBanner();
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		log.info("App context destroyed: " + applicationPath(sce));
	}

	private void showContextInfo(ServletContextEvent sce) {
		log.info("App context initialized: " + applicationPath(sce));
		log.info(sce.getServletContext().getServerInfo());
	}

	private String applicationPath(ServletContextEvent sce) {
		return sce.getServletContext().getRealPath("");
	}

	
}
