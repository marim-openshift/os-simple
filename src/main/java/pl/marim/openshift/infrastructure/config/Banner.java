package pl.marim.openshift.infrastructure.config;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Banner {

	void showBanner() {
		InputStream bannerStream = ApplicationContextListener.class.getResourceAsStream("/banner.txt");
		StringBuilder bannerBuilder = new StringBuilder();
	    try (Scanner scanner = new Scanner(bannerStream, StandardCharsets.UTF_8.name())) {
	        String text = scanner.useDelimiter("\\A").next();
	        if (isNotBlank(text)) {
	        	bannerBuilder.append(text).append("\n");
	        }
	    }
	    log.info("\n{}", bannerBuilder.toString());
	}
}
