package pl.marim.openshift.infrastructure.lang;

import static java.time.format.DateTimeFormatter.ofPattern;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;


public final class Dates {

	private static final DateTimeFormatter CUSTOMER_ACQ_DATE_FORMAT = ofPattern("dd/MM/yyyy");
	private static final DateTimeFormatter DEFAULT_DATE_FORMAT = ofPattern("yyyy-MM-dd");

	private Dates() {}
	
	public static String formatCustAcq(LocalDate localDate) {
		return localDate.format(CUSTOMER_ACQ_DATE_FORMAT);
	}
	
	public static LocalDate dateFrom(String dateAsString) {
		return dateFrom(dateAsString, DEFAULT_DATE_FORMAT);
	}
	
	public static LocalDate dateFrom(String dateAsString, DateTimeFormatter format) {
		return LocalDate.parse(dateAsString, format);
	}
	
	public static String format(LocalDate date) {
		return date.format(DEFAULT_DATE_FORMAT);
	}
	
	public static String format(LocalDate date, DateTimeFormatter formatter) {
		return date.format(formatter);
	}
	
	public static LocalDateTime convertToLocalDateTime(Date dateToConvert) {
	    return dateToConvert.toInstant()
	    					.atZone(ZoneId.systemDefault())
	    					.toLocalDateTime();
	}	
}
