package pl.marim.openshift.infrastructure.exceptions;

public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ServiceException(String msg) {
		super(msg);
	}
	
	public ServiceException(Exception e) {
		super(e);
	}
	
	public ServiceException(String msg, Exception e ) {
		super(msg, e);
	}
}
