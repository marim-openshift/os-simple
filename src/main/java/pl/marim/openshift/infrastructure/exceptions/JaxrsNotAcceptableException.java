package pl.marim.openshift.infrastructure.exceptions;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Provider
public class JaxrsNotAcceptableException implements ExceptionMapper<NotAcceptableException> {

	@Context 
	private HttpServletRequest httpServletRequest;

	@Override
	public Response toResponse(NotAcceptableException e) {
		log.error(e.getMessage(), e);
		return Response
				.status(Response.Status.INTERNAL_SERVER_ERROR)
				.header("Reason", e.getMessage())
				.build();
	}

	
}
