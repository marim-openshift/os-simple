package pl.marim.openshift.infrastructure.jaxrs;

import java.io.IOException;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import pl.marim.openshift.infrastructure.lang.Dates;

public class JsonDateSerializer extends JsonSerializer<LocalDate> {

	@Override
	public void serialize(LocalDate date, JsonGenerator generator, SerializerProvider provider) throws IOException {
		String dateString =  Dates.format(date);
		generator.writeString(dateString);
	}
}