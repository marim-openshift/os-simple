package pl.marim.openshift.infrastructure.health;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;

@Health
public class ApplicationHealth implements HealthCheck {

	@Override
	public HealthCheckResponse call() {
		return HealthCheckResponse
				.named("openshift-sample-api")
				.withData("Application state", "OK")
				.up()
				.build();
	}
}
