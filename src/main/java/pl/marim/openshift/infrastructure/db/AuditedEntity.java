package pl.marim.openshift.infrastructure.db;

import java.time.LocalDateTime;

public interface AuditedEntity {

	void setCreated(LocalDateTime dateTime);
	void setModified(LocalDateTime date);
	
}
