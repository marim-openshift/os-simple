package pl.marim.openshift.infrastructure.db;


import static javax.transaction.Transactional.TxType.MANDATORY;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import pl.marim.openshift.infrastructure.exceptions.ServiceException;

@Transactional(MANDATORY)
public abstract class DbService {

	@Inject
	protected EntityManager em;
	
	/**
	 * Expects no results (return null) or exactly one.
	 * Otherwise throws exception.
	 * @param queryResult
	 * @return
	 */
	protected <T> T singleResult(List<T> queryResult) {
		if (queryResult.size() > 1) {
			throw new ServiceException("Found more one object of: " + queryResult.iterator().next().getClass());
		}
		if (queryResult.iterator().hasNext()) {
			return queryResult.iterator().next();
		} else {
			return null;
		}
	}
	
	protected <T> Optional<T> findOne(List<T> queryResult) {
		if (queryResult.size() > 1) {
			throw new ServiceException("Found more one object of: " + queryResult.iterator().next().getClass());
		}
		if (queryResult.iterator().hasNext()) {
			return Optional.of(queryResult.iterator().next());
		} else {
			return Optional.empty();
		}
	}
	

	/**
	 * If no results returns null, else returns first from list.
	 */
	protected <T> T firstResult(List<T> queryResult) {
		if (queryResult.iterator().hasNext()) {
			return queryResult.iterator().next();
		} else {
			return null;
		}
	}
	
	/**
	 * Merge zwraca nowa instancje, wiec po wywolaniu 
	 * tej metody trzeba korzystac ze zwroconego obiektu,
	 * ktory jest zarzadzany przez entityManager.
	 */
	protected <T> T mergeAndGet(T entity) {
		T merged = em.merge(entity);
		em.flush();
		return merged;
	}
	
	protected void persistAndRefresh(Object entity) {
		em.persist(entity);
		em.refresh(entity);
		em.flush();
	}

	protected void persist(Object entity) {
		em.persist(entity);
		em.flush();
	}
	
	protected void refresh(Object entity) {
		em.refresh(entity);
	}
	
	protected void removeAll(Collection<? extends Object> entities) {
		for (Object entity : entities) {
			em.remove(entity);
		}
		em.flush();
	}
	
	protected <E> void remove(E entity) {
		if (em.contains(entity)) {
			em.remove(entity);
		} else {
			em.merge(entity);
			em.remove(entity);
		}
		em.flush();
	}
	
	public void updateModificationInfo(AuditedEntity entityWithAudit) {
		entityWithAudit.setModified(LocalDateTime.now());
	}
	
	public void updateAuditInfo(AuditedEntity entityWithAudit) {
		entityWithAudit.setCreated(LocalDateTime.now());
		entityWithAudit.setModified(LocalDateTime.now());
	}
}
