package pl.marim.openshift.infrastructure.stats;

import static org.apache.commons.lang3.time.StopWatch.createStarted;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.commons.lang3.time.StopWatch;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Stopwatch
@Interceptor
public class StopwatchInterceptor {

	@AroundInvoke
	public Object stopwatchExecution(InvocationContext context) throws Exception {
		Class<?> clazz = context.getTarget().getClass();
		Method method = context.getMethod();
		StopWatch stopwatch = createStarted();
		Object methodResult = context.proceed();
		log.info("[TIME] {}.{} ms: {}", clazz.getSimpleName(), method.getName(), stopwatch.getTime(TimeUnit.MILLISECONDS));
		return methodResult;
	}

}
