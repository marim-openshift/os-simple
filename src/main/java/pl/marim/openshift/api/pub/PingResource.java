package pl.marim.openshift.api.pub;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import pl.marim.openshift.infrastructure.exceptions.ServiceException;

@Slf4j
@Path("/pub/ping")
@Produces(MediaType.APPLICATION_JSON)
public class PingResource {

	@GET
	@Operation(hidden = true)
	public Response ping() {
		log.debug("Ping called");
		return Response.ok(new PingData("test-ping-app", System.currentTimeMillis())).build();
	}

	@GET
	@Path("/error")
	@Operation(hidden = true)
	public Response pingError() {
		log.debug("Ping error called");
		throw new ServiceException("Requested ping error");
	}

	@Data
	@AllArgsConstructor
	public static class PingData {
		private String application;
		private long timestamp;
	}
}
