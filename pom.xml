<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>pl.marim.openshift.tests</groupId>
	<artifactId>os-simple</artifactId>
	<version>1.0-SNAPSHOT</version>
	<packaging>war</packaging>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.compiler.source>11</maven.compiler.source>
		<maven.compiler.target>11</maven.compiler.target>
		<failOnMissingWebXml>false</failOnMissingWebXml>

		<!-- Application dependencies -->
		<flyway.version>5.2.4</flyway.version>
		<commons-lang3.version>3.8.1</commons-lang3.version>
		<commons-io.version>2.6</commons-io.version>

		<!-- java base -->
		<java.vendor>redhat</java.vendor>
		<java.version>11</java.version>

		<!-- jakarta with extensions -->
		<jee.version>8.0</jee.version>
		<microprofile.version>1.3</microprofile.version>

		<!-- test base -->
		<junit.platform.version>1.1.0</junit.platform.version>
		<junit.jupiter.version>5.1.0</junit.jupiter.version>
		<assertj.version>3.4.1</assertj.version>

		<!-- logging base -->
		<logback.version>1.2.3</logback.version>
		<slf4j.version>1.7.25</slf4j.version>

		<!-- utils -->
		<lombok.version>1.18.6</lombok.version>
		<common.beanutils.version>1.9.3</common.beanutils.version>
		
		<!-- tests settings -->
		<skipIT>true</skipIT>

		<!-- Plugins -->
		<maven.compiler.plugin.version>3.8.0</maven.compiler.plugin.version>
		<maven.surefire.plugin.version>2.22.1</maven.surefire.plugin.version>
		<maven.dependency.plugin.version>3.0.1</maven.dependency.plugin.version>
		<maven.failsafe.plugin.version>3.0.0-M3</maven.failsafe.plugin.version>
		<maven.buildhelper.plugin.version>3.0.0</maven.buildhelper.plugin.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<version>${jee.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.eclipse.microprofile</groupId>
			<artifactId>microprofile</artifactId>
			<version>${microprofile.version}</version>
			<type>pom</type>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>${commons-lang3.version}</version>
		</dependency>

		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>${commons-io.version}</version>
		</dependency>
		
		<dependency>
		    <groupId>commons-beanutils</groupId>
		    <artifactId>commons-beanutils</artifactId>
		    <version>${common.beanutils.version}</version>
		</dependency>		

		<dependency>
		    <groupId>com.airhacks</groupId>
		    <artifactId>porcupine</artifactId>
		    <version>0.0.4</version>
		</dependency>
		
		<dependency>
			<groupId>io.swagger.core.v3</groupId>
			<artifactId>swagger-annotations</artifactId>
			<version>2.0.6</version>
		</dependency>
		
		<dependency>
			<!-- bean validation in tests -->
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-validator</artifactId>
			<version>5.2.4.Final</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>${logback.version}</version>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>${slf4j.version}</version>
			<scope>provided</scope>
		</dependency>
		
		<dependency>
			<groupId>org.flywaydb</groupId>
			<artifactId>flyway-core</artifactId>
			<version>${flyway.version}</version>
		</dependency>

		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${lombok.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<version>${junit.jupiter.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.assertj</groupId>
			<artifactId>assertj-core</artifactId>
			<version>${assertj.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- potrzebne do jax-rs client -->
		<dependency>
			<groupId>org.glassfish.jersey.core</groupId>
			<artifactId>jersey-client</artifactId>
			<version>2.25.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.media</groupId>
			<artifactId>jersey-media-json-jackson</artifactId>
			<version>2.25.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
			<version>2.9.8</version>
			<scope>provided</scope>
		</dependency>

		<!-- removed EE libraries from java 11 -->
		<dependency>
			<groupId>javax.xml.ws</groupId>
			<artifactId>jaxws-api</artifactId>
			<version>2.3.1</version>
		</dependency>
		<dependency>
			<groupId>javax.jws</groupId>
			<artifactId>javax.jws-api</artifactId>
			<version>1.1</version>
		</dependency>
		
		<dependency>
			<groupId>javax.xml.bind</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>2.3.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.sun.xml.bind</groupId>
			<artifactId>jaxb-core</artifactId>
			<version>2.3.0.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.sun.xml.bind</groupId>
			<artifactId>jaxb-impl</artifactId>
			<version>2.3.2</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.activation</groupId>
			<artifactId>activation</artifactId>
			<version>1.1.1</version>
			<scope>provided</scope>
		</dependency>
	</dependencies>

	<build>
		<finalName>os-simple</finalName>
		<plugins>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.0</version>
	  		</plugin>
			<plugin>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>${maven.surefire.plugin.version}</version>				
			</plugin>
			<plugin>
			  <groupId>org.apache.maven.plugins</groupId>
			  <artifactId>maven-failsafe-plugin</artifactId>
			  <version>${maven.failsafe.plugin.version}</version>
              <configuration>
			  	<skipITs>${skipIT}</skipITs>
              </configuration>			  
			  <executions>
			    <execution>
			      <id>integration-test</id>
			      <goals>
			        <goal>integration-test</goal>
			        <goal>verify</goal>
			      </goals>
			    </execution>
			  </executions>
			</plugin>
			<plugin>
				<artifactId>maven-toolchains-plugin</artifactId>
				<configuration>
					<toolchains>
						<jdk>
							<version>${java.version}</version>
							<vendor>${java.vendor}</vendor>
						</jdk>
					</toolchains>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>toolchain</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											org.apache.maven.plugins
										</groupId>
										<artifactId>
											maven-toolchains-plugin
										</artifactId>
										<versionRange>
											[1.1,)
										</versionRange>
										<goals>
											<goal>toolchain</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											io.swagger.core.v3
										</groupId>
										<artifactId>
											swagger-maven-plugin
										</artifactId>
										<versionRange>
											[2.0.6,)
										</versionRange>
										<goals>
											<goal>resolve</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<profiles>
		<profile>
			<id>dev</id>			
			<dependencies>
				<!-- swagger core openapi.json generation -->
				<dependency>
					<groupId>io.swagger.core.v3</groupId>
					<artifactId>swagger-jaxrs2</artifactId>
					<version>2.0.6</version>
				</dependency>
				<dependency>
					<groupId>io.swagger.core.v3</groupId>
					<artifactId>swagger-jaxrs2-servlet-initializer</artifactId>
					<version>2.0.6</version>
				</dependency>

				<!-- swagger-ui integration -->
				<dependency>
					<groupId>org.webjars</groupId>
					<artifactId>swagger-ui</artifactId>
					<version>3.17.4</version>
				</dependency>
			</dependencies>
		</profile>
		<profile>
			<id>swagger</id>
			<build>
				<plugins>
					<plugin>
		                <groupId>io.swagger.core.v3</groupId>
		                <artifactId>swagger-maven-plugin</artifactId>
		                <version>2.0.7</version>
		                <configuration>
		                    <outputFileName>openapi</outputFileName>
		                    <outputPath>${project.build.directory}/generatedtest</outputPath>
		                    <outputFormat>JSONANDYAML</outputFormat>
		                    <resourcePackages>
		                        <package>pl.marim.openshift</package>
		                    </resourcePackages>
		                    <prettyPrint>TRUE</prettyPrint>
		                </configuration>
		                <executions>
		                    <execution>
		                        <phase>compile</phase>
		                        <goals>
		                            <goal>resolve</goal>
		                        </goals>
		                    </execution>
		                </executions>
		            </plugin>	  
            	</plugins>
			</build>
		</profile>
	</profiles>
</project>
