FROM jboss/wildfly:15.0.1.Final

LABEL pl.marim.openshift.version="1.0.0"
LABEL pl.marim.openshift.release-date="2019-02-01"
LABEL author="Mariusz Materna"
LABEL email="mariusz.materna@gmail.com"
LABEL vendor1="marim"


RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent

RUN rm -rf /opt/jboss/wildfly/standalone/configuration/standalone_xml_history/current

# application deploy
ADD target/os-simple.war /opt/jboss/wildfly/standalone/deployments/

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]